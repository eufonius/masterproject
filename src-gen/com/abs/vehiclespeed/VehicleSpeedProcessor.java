/*
* This is Java interface of the component VehicleSpeedProcessor
*/
		
package com.abs.vehiclespeed;
		
import java.util.*;
		
public interface VehicleSpeedProcessor {public  Integer  vhcSpeedProcService (
    		            Integer whlSpeedFRInp, 
    		    		Integer whlSpeedFLInp
    		 ); 
}