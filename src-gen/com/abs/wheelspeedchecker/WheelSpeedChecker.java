/*
* This is Java interface of the component WheelSpeedChecker
*/
		
package com.abs.wheelspeedchecker;
		
import java.util.*;
		
public interface WheelSpeedChecker {
	public  Map<String, Object>  WSCheckerService (Integer currWheelFRSpeed, Integer currWheelFLSpeed); 
}