/*
		* This is Java interface of the component ABSSafetyStateMgr
		*/
		
		package com.abs.abssafetystatemgr;
		
		import java.util.*;
		
		public interface ABSSafetyStateMgr {public  Integer  RunABSSafeStMgrService (
    		Integer whlSpeedState, 
    		    		Integer brakePdlForceState
    		 ); 
}