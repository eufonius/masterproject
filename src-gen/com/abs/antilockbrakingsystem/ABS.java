/*
* This is Java interface of the component ABS
*/
		
package com.abs.antilockbrakingsystem;
		
import java.util.*;
		
public interface ABS {
	public  Integer  RunABSService (Integer brakeTrqInp, Integer vhcSpeedInp, Integer whlSpeedInp, Integer activationState); 
}