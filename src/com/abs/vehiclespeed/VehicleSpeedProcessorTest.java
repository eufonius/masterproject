package com.abs.vehiclespeed;

import static org.junit.Assert.*;

import org.junit.Test;

public class VehicleSpeedProcessorTest {

	@Test
	public void testVhcSpeedProcService() {
		VehicleSpeedProcessorImpl VSPTest = new VehicleSpeedProcessorImpl();
		Integer result = VSPTest.vhcSpeedProcService(100, 100);
		assertTrue(result == 7);	
	}
}
