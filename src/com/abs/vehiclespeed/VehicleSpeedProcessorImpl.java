/*
* This is Java implementation of the atomic component VehicleSpeedProcessor
*/
		
package com.abs.vehiclespeed;
		
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
		
public class VehicleSpeedProcessorImpl implements VehicleSpeedProcessor {
    /**
     * Constructor of atomic component VehicleSpeedProcessor
     */
	
     public VehicleSpeedProcessorImpl() {
     }

     public  Integer  vhcSpeedProcService (Integer whlSpeedFRInp, Integer whlSpeedFLInp) {
    			Integer vhcSpeedOup = null;
    			
    			/*Wheel Speed receive in RPM unit
    			 * Adaptation calculation from: 
    			 * http://uk.mathworks.com/help/simulink/examples/modeling-an-anti-lock-braking-system.html
    			 * http://www.ehow.co.uk/how_7445086_calculate-wheel-speed-tire-diameter.html
    			 * http://ncalculators.com/area-volume/circle-calculator.htm
    			 * http://petersmagnusson.org/2009/09/15/why-are-there-5280-feet-in-a-mile/
    			 * 1 foot = 12 inches
    			 * pi = 3.1416
    			 * circle circumference = 2 pi r 
    			 * Standard tire size is 205/55R16 equivalence to 25 inches in diameter
    			 * based on http://tire-size-conversion.com/tire-size-calculator/
    			 * http://www.wheel-size.com/ */
    			
    			//declare variables
    			double dblCircleCfrnc = 0.0;
    			double fnlDblCircleCfrnc = 0;
    			double milesCircleCfrnc = 0.0;
    			double dblWheelFLSpeed = 0.0;
    			double dblWheelFRSpeed = 0.0;
    			int intWheelSpeedFL = 0;
    			int intWheelSpeedFR = 0;
    			
    			/*Calculate circle circumference in inches
    			  circle circumference formula = 2*pi*r 
    			  standard diameter for car tire is 25 inches in diameter*/
    			dblCircleCfrnc = 2 * 3.1416 * 12.5;
 
    			
    			/*convert from inches to feet
    			 1 foot = 12 inches*/
    			fnlDblCircleCfrnc = dblCircleCfrnc / 12;
    			
    			/*convert from feet to mile to get the correlation in mile and revolution per minute (RPM)
    			 * 1 mile = 5280 feet */
    			milesCircleCfrnc = 5280 / fnlDblCircleCfrnc;
    			
    			
    			/*Get vehicle speed by dividing wheel speed with tire revolution per mile
    			 *if FL wheel speed equivalence with FR wheel speed, take 1 wheel speed for
    			 *vehicle speed.
    			 *By default, get wheel speed on the left and multiply with 60 minutes */
    			if (whlSpeedFLInp == whlSpeedFRInp){
    				dblWheelFLSpeed = whlSpeedFLInp / milesCircleCfrnc;
    				dblWheelFLSpeed = dblWheelFLSpeed * 60;
    				
    				//round to the nearest decimal point
    				dblWheelFLSpeed = Math.round(dblWheelFLSpeed);
    				
    				//convert to integer from double data type
    				intWheelSpeedFL = (int)(dblWheelFLSpeed);
    				
    				//assign the output 
    				vhcSpeedOup = intWheelSpeedFL;
    				
    			}else if (whlSpeedFLInp != whlSpeedFRInp ){
    				//calculate both wheel speed to determine the vehicle speed
    				//compare both value and get the highest value to assign as vehicle speed
    				dblWheelFLSpeed = whlSpeedFLInp / milesCircleCfrnc;
    				dblWheelFLSpeed = dblWheelFLSpeed * 60;
    				
    				//round to the nearest decimal point
    				dblWheelFLSpeed = Math.round(dblWheelFLSpeed);
    				
    				//convert to integer from double data type
    				intWheelSpeedFL = (int)(dblWheelFLSpeed);
    				
    				
    				
    				//Calculate FR Wheel Speed
    				dblWheelFRSpeed = whlSpeedFRInp / milesCircleCfrnc;
    				dblWheelFRSpeed = dblWheelFRSpeed * 60;
    				
    				//round to the nearest decimal point
    				dblWheelFRSpeed = Math.round(dblWheelFRSpeed);
    				
    				//convert to integer from double data type
    				intWheelSpeedFR = (int)(dblWheelFRSpeed);
    				
    				//assign vehicle speed according to the highest vehicle speed
    				if (intWheelSpeedFL > intWheelSpeedFR){
    					vhcSpeedOup = intWheelSpeedFL;
    				}else{
    					vhcSpeedOup = intWheelSpeedFR;
    				}
    			}
    			
    			return vhcSpeedOup;
    		}
}