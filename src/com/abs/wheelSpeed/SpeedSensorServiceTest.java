package com.abs.wheelSpeed;

import static org.junit.Assert.*;

import org.junit.Test;

public class SpeedSensorServiceTest {

	@Test
	public void testForSpeedSensor() {
		WheelSpeedSensorImpl WSSTest = new WheelSpeedSensorImpl();
		Integer WSSResult = WSSTest.speedSensorService(10);
		assertTrue(WSSResult == 10);
	}
}
