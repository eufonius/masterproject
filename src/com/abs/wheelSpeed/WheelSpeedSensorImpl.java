/*
 * This is Java implementation of the atomic component WheelSpeedSensor
 */

package com.abs.wheelSpeed;

import java.util.*;

public class WheelSpeedSensorImpl implements WheelSpeedSensor {
	/**
	 * Constructor of atomic component WheelSpeedSensor
	 */
	public WheelSpeedSensorImpl() {
	}

	public Integer speedSensorService(Integer spdSnsrInp) {
		Integer spdSnsrOup = null;
		// Wheel Speed unit = Revolution per minute (RPM)

		/*
		 * Get input from the main service and assign to Wheel Speed Sensor The
		 * input value is in the decimal format. In AUTOSAR, it received 4 bits
		 * data from the bus, thus this component can be used to implement the
		 * conversion from bit to decimal. For this study, this component
		 * intentionally receive in decimal point
		 */

		spdSnsrOup = spdSnsrInp;

		return spdSnsrOup;
	}
}