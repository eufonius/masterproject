/*
* This is Java implementation of the atomic component BrakeController
*/
		
package com.abs.brakecontroller;
		
import java.util.*;
		
public class BrakeControllerImpl implements BrakeController {
    /**
     * Constructor of atomic component BrakeController
     */
	
     public BrakeControllerImpl() {
     }
     
     public  Map<String, Object>  brakeControllerService (Integer brakePdlPosInp) {
    			Map<String, Object> returnValues = new HashMap<String, Object>();
    			Integer brakeTrqFLOup = null;
    			Integer brakeTrqFROup = null;
    			
    			// TODO: auto-generated code, to be implemented by developers
    			
    			//Assigned the brakePdlInput directly to the brakeTrqFLOup and brakeTrgFROup
    			brakeTrqFLOup = brakePdlPosInp;
    			brakeTrqFROup = brakePdlPosInp;
    			
                returnValues.put("brakeTrqFLOup", brakeTrqFLOup);
    			returnValues.put("brakeTrqFROup", brakeTrqFROup);
    			
    			return returnValues;
    		}
}