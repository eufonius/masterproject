package com.abs.brakepedalsensor;

import static org.junit.Assert.*;

import org.junit.Test;

public class BrakePdlSensorServiceTest {

	@Test
	public void testForBrakePdlSensor() {
		BrakePedalSensorImpl BPSTest = new BrakePedalSensorImpl();
		Integer result = BPSTest.brakePdlSensorService(10);
		assertTrue(result == 10);		
	}
}
