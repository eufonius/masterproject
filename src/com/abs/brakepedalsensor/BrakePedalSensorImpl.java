/*
 * This is Java implementation of the atomic component BrakePedalSensor
 */
		
package com.abs.brakepedalsensor;
		
import java.util.*;
		
public class BrakePedalSensorImpl implements BrakePedalSensor {
    /**
     * Constructor of atomic component BrakePedalSensor
     */
   
	 public BrakePedalSensorImpl() {
     }

     public  Integer  brakePdlSensorService (Integer brakeSensorInp) {
    			Integer brakePdlPosOup = null;
    			// TODO: auto-generated code, to be implemented by developers
    			
    			/*Get input from the main service and assign to Brake Pedal Sensor
  			      The input value is in the decimal format.
  			      In AUTOSAR, it received 4 bits data from the bus, thus this component can be used
  			      to implement the conversion from bit to decimal.
  			      For this study, this component intentionally receive in decimal point*/
    			
    			brakePdlPosOup = brakeSensorInp;
    			
    			return brakePdlPosOup;
     }
}